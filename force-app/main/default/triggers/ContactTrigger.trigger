/**
 * Created by Dhananjay on 28/12/20.
 */

trigger ContactTrigger on Contact (before insert) {
for(Contact con : Trigger.new){
    if(con.Phone == null && con.MobilePhone == null) {
        con.addError('One of either phone or mobile phone must be provided');
    }
    else if (con.Phone == null && con.MobilePhone != null)
    {
        con.Phone = con.MobilePhone;
    }
}
}