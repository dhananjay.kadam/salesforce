/**
 * @description       :
 * @author            : Amit Singh
 * @group             :
 * @last modified on  : 01-01-2021
 * @last modified by  : Amit Singh
 * Modifications Log
 * Ver   Date         Author       Modification
 * 1.0   12-31-2020   Amit Singh   Initial Version
 **/
public class EinsteinOCRService {
  public static final String OCR_API = 'https://api.einstein.ai/v2/vision/ocr';
  public static final String OCR_MODEL = 'OCRModel';
  public static final String OCR_MODEL_TABLE = 'OCRModel';

  public static void readTextFromImageByURL() {
    String sample = 'https://files.readme.io/7e0ac53-emergency-evacuation-route-signpost.jpg';
    String result = EinsteinAPIService.imageOCR(
      OCR_API,
      sample,
      OCR_MODEL,
      false,
      false
    );

    parseResponse(result);
  }

  public static void readTextFromImageByBase64(String recordId) {
    List<ContentDocumentLink> contentLink = [
      SELECT ContentDocumentId, LinkedEntityId
      FROM ContentDocumentLink
      WHERE LinkedEntityId = :recordId
      WITH SECURITY_ENFORCED
    ];
    if (!contentLink.isEmpty()) {
      ContentVersion content = [
        SELECT Title, VersionData
        FROM ContentVersion
        WHERE ContentDocumentId = :contentLink.get(0).ContentDocumentId
        WITH SECURITY_ENFORCED
        LIMIT 1
      ];
      String sample = EncodingUtil.base64Encode(content.VersionData);
      //System.debug('Sample' + sample);
      String result = EinsteinAPIService.imageOCR(
        OCR_API,
        sample,
        OCR_MODEL,
        true,
        false
      );
      //System.debug(result);
      parseResponse(result);
    }
  }
  private static void parseResponse(String result) {
    EinsteinOCRResponse response = (EinsteinOCRResponse) System.JSON.deserialize(
      result,
      EinsteinOCRResponse.class
    );
    for (EinsteinOCRResponse.Probabilities prob : response.probabilities) {
      System.debug(System.LoggingLevel.DEBUG, prob.label);
    }
  }
}
