/**
 * @description       :
 * @author            : Amit Singh
 * @group             :
 * @last modified on  : 12-30-2020
 * @last modified by  : Amit Singh
 * Modifications Log
 * Ver   Date         Author       Modification
 * 1.0   12-30-2020   Amit Singh   Initial Version
 **/
public with sharing class EinsteinAPIService {
  public static final String OAUTH_END_POINT = 'https://api.einstein.ai/v2/oauth2/token';

  public static String getAccessToken() {
    /* Get the key file from the File Object that we have downloaded from Einstein platform API*/
    ContentVersion base64Content = [
      SELECT Title, VersionData
      FROM ContentVersion
      WHERE Title = 'einstein_platform' OR Title = 'predictive_services'
      ORDER BY Title
      LIMIT 1
    ];

    String keyContents = base64Content.VersionData.toString();
    keyContents = keyContents.replace('-----BEGIN RSA PRIVATE KEY-----', '');
    keyContents = keyContents.replace('-----END RSA PRIVATE KEY-----', '');
    keyContents = keyContents.replace('\n', '');
    //System.debug(keyContents);

    // Get a new token
    JWT jwt = new JWT('RS256');
    // jwt.cert = 'JWTCert'; // openssl
    jwt.pkcs8 = keyContents; // Comment this if you are using jwt.cert
    jwt.iss = 'developer.force.com';
    jwt.sub = 'dhananjay.kadam@gmail.com';
    jwt.aud = OAUTH_END_POINT;
    jwt.exp = '3600';
    String access_token = JWTBearerFlow.getAccessToken(OAUTH_END_POINT, jwt);
    System.debug(access_token);
    return access_token;
  }
  public static String imageOCR(
    String endPoint,
    String sample,
    String model,
    Boolean isBase64,
    Boolean isCard
  ) {
    String result = einsteinAPICall(endPoint, sample, model, isBase64, isCard);
    return result;
  }
  public static String predictImage(
    String endPoint,
    String sample,
    String model,
    Boolean isBase64,
    Boolean isCard
  ) {
    String result = einsteinAPICall(endPoint, sample, model, isBase64, isCard);
    return result;
  }
  private static String einsteinAPICall(
    String endPoint,
    String sample,
    String model,
    Boolean isBase64,
    Boolean isCard
  ) {
    String contentType = HttpFormBuilder.GetContentType();
    String access_token = getAccessToken();

    //  Compose the form
    String form64 = '';

    form64 += HttpFormBuilder.WriteBoundary();
    form64 += HttpFormBuilder.WriteBodyParameter(
      'modelId',
      EncodingUtil.urlEncode(model, 'UTF-8')
    );
    form64 += HttpFormBuilder.WriteBoundary();
    if (isCard) {
      form64 += HttpFormBuilder.WriteBodyParameter(
        'task',
        EncodingUtil.urlEncode('contact', 'UTF-8')
      );
      form64 += HttpFormBuilder.WriteBoundary();
    }
    if (isBase64) {
      form64 += HttpFormBuilder.WriteBodyParameter(
        'sampleBase64Content',
        sample
      );
    } else {
      form64 += HttpFormBuilder.WriteBodyParameter('sampleLocation', sample);
    }

    form64 += HttpFormBuilder.WriteBoundary(HttpFormBuilder.EndingType.CrLf);

    Blob formBlob = EncodingUtil.base64Decode(form64);
    String contentLength = String.valueOf(formBlob.size());

    HttpRequest httpRequest = new HttpRequest();

    httpRequest.setBodyAsBlob(formBlob);
    httpRequest.setHeader('Connection', 'keep-alive');
    httpRequest.setHeader('Content-Length', contentLength);
    httpRequest.setHeader('Content-Type', contentType);
    httpRequest.setMethod('POST');
    httpRequest.setTimeout(120000);
    httpRequest.setHeader('Authorization', 'Bearer ' + access_token);
    httpRequest.setEndpoint(endPoint);

    Http http = new Http();
    try {
      HttpResponse res = http.send(httpRequest);
      System.debug(
        System.LoggingLevel.DEBUG,
        ' Response From Einstein Platform API ' + res.getBody()
      );
      if (res.getStatusCode() == 200) {
        return res.getBody();
      }
    } catch (System.CalloutException e) {
      System.debug('ERROR:' + e);
      return e.getStackTraceString();
    }
    return null;
  }
}
