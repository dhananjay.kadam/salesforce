/**
 * Created by 607206485 on 5/10/2021.
 */

@RestResource(UrlMapping='/v1/Welcome/*')
global with sharing class SampleRestCallOut {
  global final static String WELCOME_MESSAGE = 'Welcome to First Rest API Call';

  @HttpGet
  global static String getWelcomeMessage() {
    return WELCOME_MESSAGE;
  }

  @HttpPost
  global static String postWelcomeMessage(String message) {
    return WELCOME_MESSAGE + ' ' + message;
  }
}
