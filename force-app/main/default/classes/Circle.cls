/**
 * Created by 607206485 on 1/11/2021.
 */

public with sharing class Circle implements Shape{

    private Double redis = 0;

    public Circle(Double r)
    {
        redis = r;
    }

    public Double getArea()
    {
        return Math.PI * redis * redis;
    }

}