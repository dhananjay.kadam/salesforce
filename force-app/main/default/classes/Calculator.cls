/**
 * Created by 607206485 on 1/14/2021.
 */

public with sharing class Calculator {

    public static Integer addNumbers(Integer i, Integer j)
    {
        if(i == null || j == null)
        {
            throw new CalculatorException('You cannot use null as a parameter');
        }
        return i + j;
    }

}