/**
 * Created by 607206485 on 5/11/2021.
 */

global class SSOEventCondition implements TxnSecurity.EventCondition {
  public Boolean evaluate(SObject event) {
    switch on event {
      when LoginEvent loginEvent {
        return evaluate(loginEvent);
      }
      when null {
        return false;
      }
      when else {
        return false;
      }
    }
  }
  private Boolean evaluate(LoginEvent loginEvent) {
    String userName = loginEvent.Username;
    String EIN = userName.substringBefore('@');
    //System.debug('The EIN is :'+ EIN);
    String regEx = '^\\d{9}$';
    Pattern myPattern = Pattern.compile(regEx);
    Matcher myMatcher = myPattern.matcher(EIN);
    Boolean result = myMatcher.matches();
    //System.debug('The RegEx Pattern is matching :' + result);

    if (loginEvent.LoginType != 'SAML Sfdc Initiated SSO' && result == true) {
      return true;
    }
    return false;
  }
}
