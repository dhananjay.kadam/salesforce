public class FreshdeskUtil {
  public static final String TICKET_ENDPOINT = 'https://newaccount1621949495315.freshdesk.com/api/v2/tickets';
  public static final String CONTACT_ENDPOINT = 'https://newaccount1621949495315.freshdesk.com/api/v2/contacts';

  public static void createContact(
    String name,
    String email,
    String phone,
    String mobile
  ) {
    String errorMessage = '';
    String endPoint = CONTACT_ENDPOINT;

    String cred = System.Label.Freshdesk; // base64Encode(username:password)

    String requestBody =
      '{' +
      '"name": "' +
      name +
      '",' +
      '"email": "' +
      email +
      '",' +
      '"phone" : "' +
      phone +
      '",' +
      '"mobile" : "' +
      mobile +
      '"' +
      '}';

    Http http = new Http();
    HttpRequest httpReq = new HttpRequest();

    httpReq.setMethod('POST');
    httpReq.setHeader(
      'Authorization',
      'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(cred))
    );
    httpReq.setEndpoint(endPoint);

    httpReq.setBody(requestBody);

    httpReq.setHeader('Content-Type', 'application/json');
    HttpResponse response = new HttpResponse();

    try {
      response = http.send(httpReq);
      if (response.getStatusCode() == 201) {
        String responseBody = response.getBody();
        System.debug(
          System.LoggingLevel.DEBUG,
          ' Response from Server ' + responseBody
        );
      } else {
        errorMessage =
          'Unexpected Error while communicating with API. ' +
          'Status ' +
          response.getStatus() +
          ' and Status Code ' +
          response.getStatusCode();
        System.debug(
          System.LoggingLevel.DEBUG,
          'Exception Executed ' + response.getBody()
        );
      }
    } catch (System.Exception e) {
      if (String.valueOf(e.getMessage()).startsWith('Unauthorized endpoint')) {
        errorMessage =
          'Unauthorized endpoint: An Administer must go to Setup -> Administer -> Security Control ->' +
          ' Remote Site Setting and add ' +
          ' ' +
          endPoint +
          ' Endpoint';
      } else {
        errorMessage =
          'Unexpected Error while communicating with API. ' +
          'Status ' +
          response.getStatus() +
          ' and Status Code ' +
          response.getStatusCode();
      }
      System.debug(
        System.LoggingLevel.DEBUG,
        'Exception Executed ' + errorMessage
      );
    }
  }

  public static void createTicket(
    String sub,
    String email,
    String description,
    Integer priority,
    Integer status,
    Integer source
  ) {
    String errorMessage = '';
    String endPoint = TICKET_ENDPOINT;
    String cred = System.Label.Freshdesk; // base64Encode(username:password)

    String requestBody =
      '{' +
      '"description": "' +
      description +
      '",' +
      '"subject": "' +
      sub +
      '",' +
      '"email": "' +
      email +
      '",' +
      '"priority": ' +
      priority +
      ',' +
      '"status": ' +
      status +
      ',' +
      '"source": ' +
      source +
      '}';
    System.debug(' requestBody ' + requestBody);
    Http http = new Http();
    HttpRequest httpReq = new HttpRequest();
    httpReq.setMethod('POST');
    httpReq.setHeader('Content-Type', 'application/json');
    httpReq.setHeader(
      'Authorization',
      'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(cred))
    );
    httpReq.setEndpoint('callout:Freshdesk/api/v2/tickets');
    //httpReq.setEndpoint(TICKET_ENDPOINT);
    httpReq.setBody(requestBody);
    HttpResponse response = new HttpResponse();

    try {
      response = http.send(httpReq);
      if (response.getStatusCode() == 201) {
        String responseBody = response.getBody();
        System.debug(
          System.LoggingLevel.DEBUG,
          ' Response from Server ' + responseBody
        );
      } else {
        errorMessage =
          'Unexpected Error while communicating with API. ' +
          'Status ' +
          response.getStatus() +
          ' and Status Code ' +
          response.getStatusCode();
        System.debug(
          System.LoggingLevel.DEBUG,
          'Exception Executed ' + response.getBody()
        );
      }
    } catch (System.Exception e) {
      if (String.valueOf(e.getMessage()).startsWith('Unauthorized endpoint')) {
        errorMessage =
          'Unauthorized endpoint: An Administer must go to Setup -> Administer -> Security Control ->' +
          ' Remote Site Setting and add ' +
          ' ' +
          endPoint +
          ' Endpoint';
      } else {
        errorMessage =
          'Unexpected Error while communicating with API. ' +
          'Status ' +
          response.getStatus() +
          ' and Status Code ' +
          response.getStatusCode();
      }
      System.debug(
        System.LoggingLevel.DEBUG,
        'Exception Executed ' + errorMessage
      );
    }
  }

  public static void listCustomerTickets(String email) {
    String errorMessage = '';
    String endPoint =
      TICKET_ENDPOINT +
      '?email=' +
      EncodingUtil.urlEncode(email, 'UTF-8');
    String cred = System.Label.Freshdesk;

    Http http = new Http();
    HttpRequest httpReq = new HttpRequest();
    httpReq.setMethod('GET');
    httpReq.setEndpoint(endPoint);
    httpReq.setHeader('Content-Type', 'application/json');
    httpReq.setHeader(
      'Authorization',
      'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(cred))
    );
    HttpResponse response = new HttpResponse();

    try {
      response = http.send(httpReq);
      if (response.getStatusCode() == 200) {
        String body = response.getBody();
        System.debug(
          System.LoggingLevel.DEBUG,
          ' Response from Server ' + body
        );
        List<FreshdeskTicket> tickets = (List<FreshdeskTicket>) System.JSON.deserialize(
          body,
          List<FreshdeskTicket>.class
        );
        for (FreshdeskTicket ticket : tickets) {
          // upsert Fr_Ticket__c record here
        }
      } else {
        errorMessage =
          'Unexpected Error while communicating with API. ' +
          'Status ' +
          response.getStatus() +
          ' and Status Code ' +
          response.getStatusCode();
        System.debug(
          System.LoggingLevel.DEBUG,
          'Exception Executed ' + errorMessage
        );
      }
    } catch (System.Exception e) {
      if (String.valueOf(e.getMessage()).startsWith('Unauthorized endpoint')) {
        errorMessage =
          'Unauthorized endpoint: An Administer must go to Setup -> Administer -> Security Control ->' +
          ' Remote Site Setting and add ' +
          ' ' +
          endPoint +
          ' Endpoint';
      } else {
        errorMessage =
          'Unexpected Error while communicating with API. ' +
          'Status ' +
          response.getStatus() +
          ' and Status Code ' +
          response.getStatusCode();
      }
      System.debug(
        System.LoggingLevel.DEBUG,
        'Exception Executed ' + errorMessage
      );
    }
  }

  public static void listAllContacts() {
    String errorMessage = '';
    String endPoint = CONTACT_ENDPOINT;
    String cred = System.Label.Freshdesk;
    Http http = new Http();
    HttpRequest httpReq = new HttpRequest();
    httpReq.setMethod('GET');
    httpReq.setEndpoint('callout:Freshdesk/api/v2/contacts');
    httpReq.setHeader('Content-Type', 'application/json');
    httpReq.setHeader(
      'Authorization',
      'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(cred))
    );
    HttpResponse response = new HttpResponse();

    try {
      response = http.send(httpReq);
      if (response.getStatusCode() == 200) {
        String body = response.getBody();
        System.debug(
          System.LoggingLevel.DEBUG,
          ' Response from Server ' + body
        );
        List<FreshdeskContact> contacts = (List<FreshdeskContact>) System.JSON.deserialize(
          body,
          List<FreshdeskContact>.class
        );
        for (FreshdeskContact contact : contacts) {
        }
        System.debug(
          System.LoggingLevel.DEBUG,
          ' Response from Server ' + contacts
        );
      } else {
        errorMessage =
          'Unexpected Error while communicating with API. ' +
          'Status ' +
          response.getStatus() +
          ' and Status Code ' +
          response.getStatusCode();
        System.debug(
          System.LoggingLevel.DEBUG,
          'Exception Executed ' + errorMessage
        );
      }
    } catch (System.Exception e) {
      if (String.valueOf(e.getMessage()).startsWith('Unauthorized endpoint')) {
        errorMessage =
          'Unauthorized endpoint: An Administer must go to Setup -> Administer -> Security Control ->' +
          ' Remote Site Setting and add ' +
          ' ' +
          endPoint +
          ' Endpoint';
      } else {
        errorMessage =
          'Unexpected Error while communicating with API. ' +
          'Status ' +
          response.getStatus() +
          ' and Status Code ' +
          response.getStatusCode();
      }
      System.debug(
        System.LoggingLevel.DEBUG,
        'Exception Executed ' + errorMessage
      );
    }
  }

  public static void listAllTickets() {
    String errorMessage = '';
    String endPoint = TICKET_ENDPOINT;
    String cred = System.Label.Freshdesk; // base64Encode(username:password)

    Http http = new Http();
    HttpRequest httpReq = new HttpRequest();
    httpReq.setMethod('GET');
    httpReq.setEndpoint(endPoint);
    httpReq.setHeader('Content-Type', 'application/json');
    httpReq.setHeader(
      'Authorization',
      'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(cred))
    );
    HttpResponse response = new HttpResponse();

    try {
      response = http.send(httpReq);
      if (response.getStatusCode() == 200) {
        String body = response.getBody();
        System.debug(
          System.LoggingLevel.DEBUG,
          ' Response from Server ' + body
        );
        List<FreshdeskTicket> tickets = (List<FreshdeskTicket>) System.JSON.deserialize(
          body,
          List<FreshdeskTicket>.class
        );
        for (FreshdeskTicket ticket : tickets) {
          // upsert Fr_Ticket__c record here
        }
        System.debug(
          System.LoggingLevel.DEBUG,
          ' Response from Server ' + tickets
        );
      } else {
        errorMessage =
          'Unexpected Error while communicating with API. ' +
          'Status ' +
          response.getStatus() +
          ' and Status Code ' +
          response.getStatusCode();
        System.debug(
          System.LoggingLevel.DEBUG,
          'Exeception Executed ' + errorMessage
        );
      }
    } catch (System.Exception e) {
      if (String.valueOf(e.getMessage()).startsWith('Unauthorized endpoint')) {
        errorMessage =
          'Unauthorized endpoint: An Administer must go to Setup -> Administer -> Security Control ->' +
          ' Remote Site Setting and add ' +
          ' ' +
          endPoint +
          ' Endpoint';
      } else {
        errorMessage =
          'Unexpected Error while communicating with API. ' +
          'Status ' +
          response.getStatus() +
          ' and Status Code ' +
          response.getStatusCode();
      }
      System.debug(
        System.LoggingLevel.DEBUG,
        'Exception Executed ' + errorMessage
      );
    }
  }
}
