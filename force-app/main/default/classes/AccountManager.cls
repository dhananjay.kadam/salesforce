/*
     ******** Test User Credentials *********
     Username - udemy@integration.org
     Password - donotchangeme@1
*/
/*

  https://ap17.salesforce.com/services/apexrest/v1/Account/
  https://ap17.salesforce.com/services/apexrest/v1/Account/
  
  Sample Request for JSON as input ( for the wrapper class that we used )
  
  {
     "information" : {
        "accountRecord" : {
          "Name" : "JSON Account"
        },
        "contactRecord" : {
          "LastName" : "JSON",
          "FirstName" : "Contact"
        },
        "testMessage":"Contact us for any questions on your order."
     }
  }
*/

@RestResource(UrlMapping='/v1/Account/*')
global class AccountManager {
  @HttpPatch
  global static ResponseWrapper updateAccount(
    String rating,
    String accNumber,
    String description
  ) {
    // Get the Account Id from URI
    RestRequest request = RestContext.request;
    String requestURI = request.requestURI;
    String accountId = requestURI.substringAfterLast('/');
    // prepare the account rec
    List<Account> accountList = [SELECT Id FROM Account WHERE Id = :accountId];
    Account accRec;
    ResponseWrapper wrapper = new ResponseWrapper();
    if (accountList != null && accountList.size() > 0) {
      accRec = new Account(
        Id = accountId,
        Rating = rating,
        AccountNumber = accNumber,
        Description = description
      );
      update accRec;
      wrapper.accRec = accRec;
      wrapper.message = 'Account Record Updated';
    } else {
      wrapper.message = 'Account Record Not Found';
    }
    return wrapper;
  }

  @HttpDelete
  global static String deleteAccount() {
    RestRequest request = RestContext.request;
    String requestURI = request.requestURI;
    System.debug('requestURI ' + requestURI);
    String accountId = requestURI.substringAfterLast('/');

    List<Account> accList = [SELECT Id FROM Account WHERE Id = :accountId];
    if (accList != null && accList.size() > 0) {
      try {
        delete accList;
        return '{"message":"Account Record Deleted"}';
      } catch (System.Exception ex) {
        String errorMessage = ex.getMessage();
        return '{"message":"' + errorMessage + '"}';
      }
    } else {
      return '{"message":"Account Record not Found"}';
    }
  }

  @HttpPost
  global static AccountInformation postAccount(AccountInformation information) {
    Account accountRecord = information.accountRecord;
    insert accountRecord;
    Contact conRec = information.contactRecord;
    conRec.AccountId = accountRecord.Id;
    conRec.Description = information.testMessage;
    insert conRec;
    return information;
  }

  @HttpGet
  global static AccountWrapper accountInformation() {
    RestRequest request = RestContext.request;
    String requestURI = request.requestURI;
    String accountId = requestURI.substringAfterLast('/');

    List<Account> accountList = [
      SELECT Id, Name, Rating, Industry, Description, Phone, Fax
      FROM Account
      WHERE Id = :accountId
    ];

    List<Contact> contactList = [
      SELECT Id, Name, FirstName, LastName, Email, Phone
      FROM Contact
      WHERE AccountId = :accountId
    ];

    List<Case> caseList = [
      SELECT Id, CaseNumber, Subject, Description, Status, Owner.Name
      FROM Case
      WHERE AccountId = :accountId
    ];

    AccountWrapper wrapper = new AccountWrapper();

    if (!accountList.isEmpty()) {
      wrapper.accountRecord = accountList.get(0);
      wrapper.contactList = contactList;
      wrapper.caseList = caseList;
    } else {
    }

    return wrapper;
  }

  @HttpPut
  global static Account updateAccountInformation(
    String accName,
    String Rating,
    String accNumber
  ) {
    RestRequest request = RestContext.request;
    String requestURI = request.requestURI;
    String accountId = requestURI.substringAfterLast('/');

    Account acc = new Account(
      Id = accountId,
      Name = accName,
      Rating = Rating,
      AccountNumber = accNumber
    );
    update acc;

    return acc;
  }

  global class AccountInformation {
    global Account accountRecord { get; set; }
    global Contact contactRecord { get; set; }
    global String testMessage { get; set; }
  }

  global class AccountWrapper {
    global Account accountRecord;
    global List<Contact> contactList;
    global List<Case> caseList;
  }

  global class ResponseWrapper {
    global Account accRec;
    global String message;
  }
}
