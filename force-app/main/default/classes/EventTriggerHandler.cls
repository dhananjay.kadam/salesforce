public class EventTriggerHandler {
  @future(callout=true)
  public static void createEventInGoogle(Id eventId) {
    List<Event> eventList = [
      SELECT Id, StartDateTime, EndDate, EndDateTime, OwnerId, Subject
      FROM Event
      WHERE Id = :eventId
    ];
    if (!eventList.isEmpty()) {
      Event eve = eventList.get(0);
      String email = [SELECT Email FROM User WHERE Id = :eve.OwnerId].Email;

      String requestBody =
        '{' +
        '  "attendees": [' +
        '    {' +
        '      "email": "' +
        email +
        '"' +
        '    }' +
        '  ],' +
        '  "end": {' +
        '    "dateTime": "' +
        eve.EndDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss-HH:mm') +
        '"' +
        '  },' +
        '  "reminders": {' +
        '    "useDefault": true' +
        '  },' +
        '  "start": {' +
        '    "dateTime": "' +
        eve.StartDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss-HH:mm') +
        '"' +
        '  },' +
        '  "summary": "' +
        eve.Subject +
        '",' +
        '  "location": "Udemy.com Online Platform."' +
        '}';
      GoogleUtil.createGoogleEvent(requestBody);
    }
  }
}
