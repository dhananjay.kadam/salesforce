public class TicketWrapper {
  public List<String> cc_emails;
  public List<String> fwd_emails;
  public List<String> reply_cc_emails;
  public List<String> ticket_cc_emails;
  public Boolean fr_escalated;
  public Boolean spam;
  public Integer priority;
  public String requester_id;
  public Integer source;
  public Integer status;
  public String subject;
  public Integer id;
  public String due_by;
  public String fr_due_by;
  public Boolean is_escalated;
  public String description_text;
  public String created_at;
  public String updated_at;
  public Requester requester;

  public class Requester {
    public String id;
    public String name;
    public String email;
    public String mobile;
    public String phone;
  }
}
