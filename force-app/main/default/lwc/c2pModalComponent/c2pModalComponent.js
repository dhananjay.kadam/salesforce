/**
 * Created by Dhananjay on 30/03/21.
 */

import {LightningElement} from 'lwc';

export default class C2pModalComponent extends LightningElement {

    okHandler()
    {
        const myEvent = new CustomEvent('close',{
            bubbles:true,
            detail:{
                msg:"Modal closed successful!!"
            }
        })
        this.dispatchEvent(myEvent)
    }

    showFooterMessage()
    {
        console.log("Footer event has been called...")
    }

}