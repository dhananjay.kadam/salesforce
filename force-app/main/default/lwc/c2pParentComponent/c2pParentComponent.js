/**
 * Created by Dhananjay on 30/03/21.
 */

import {LightningElement} from 'lwc';

export default class C2PParentComponent extends LightningElement {
        showModal = false
        msg

    clickHandler()
    {
        this.showModal = true
    }
    closeHandler(event)

    {
        this.msg = event.detail.msg
        this.showModal= false
    }
}